import pandas as pd
from joblib import dump
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def logreg():
    df = pd.read_csv(
        "/root/projects/mlops_project/data/processed/data_features.csv"
    )

    X = df.drop("Grade", axis=1)
    y = df["Grade"]

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=9
    )

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    model = LogisticRegression()

    try:
        model.fit(X_train, y_train)
        predictions = model.predict(X_test)
        f1 = f1_score(y_test, predictions)

        dump(
            model,
            (
                "/root/projects/mlops_project/models/"
                "LogisticRegression.joblib"
            ),
        )

        return f"Модель обучена успешно.\nF1 Score: {f1}"

    except Exception as e:
        return f"Возникла ошибка:\n{e}"
