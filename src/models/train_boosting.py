import pandas as pd
from joblib import dump
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


def boosting() -> str:
    df = pd.read_csv(
        "/root/projects/mlops_project/data/processed/data_features.csv"
    )

    X = df.drop("Grade", axis=1)
    y = df["Grade"]

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    model = GradientBoostingClassifier(
        n_estimators=100, learning_rate=0.1, max_depth=3, random_state=42
    )
    try:
        model.fit(X_train, y_train)
        predictions = model.predict(X_test)
        f1 = f1_score(y_test, predictions)

        dump(
            model,
            (
                "/root/projects/mlops_project/models/"
                "GradientBoostingClassifier.joblib"
            ),
        )

        return f"Модель обучена успешно.\nF1 Score: {f1}"

    except Exception as e:
        return f"Возникла ошибка:\n{e}"
