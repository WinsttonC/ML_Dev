import pandas as pd
from joblib import load


def make_prediction(model_path: str, data_path: str) -> None:
    model = load(model_path)
    data = pd.read_csv(data_path)
    try:
        prediction = model.predict(data)
        return prediction
    except Exception as e:
        return f"Произошла ошибка: {e}"
