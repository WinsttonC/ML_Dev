import fire
from features.build_features import process_features

from data.preprocess import process_dataframe
from models.predict_model import make_prediction
from models.train_boosting import boosting
from models.train_logreg import logreg


def process_pipeline(path):
    process_dataframe(path)
    answer = process_features(path)
    return answer


def train_pipeline(model_name="boosting"):
    if model_name == "boosting":
        answer = boosting()
    elif model_name == "logreg":
        answer = logreg()

    return answer


def predict(data_path, model_name="boosting"):
    folder = "/root/projects/mlops_project/models/"
    if model_name == "boosting":
        model_path = f"{folder}GradientBoostingClassifier.joblib"
    elif model_name == "logreg":
        model_path = f"{folder}LogisticRegression.joblib"
    answer = make_prediction(model_path, data_path)

    return answer


fire.Fire()
