import numpy as np
import pandas as pd


def process_features(path) -> str:
    df = pd.read_csv(path)

    cat_col = df.loc[:, "IDH1":].columns

    def cat_to_int(raw: pd.Series) -> pd.Series:
        """Converts categorical values in a dataframe to integers.

        The function takes a pandas Series as input and converts categorical
        values in columns listed in cat_col to integers.

        Args:
            raw (pd.Series): Input pandas Series

        Returns:
            pd.Series: Output pandas Series with categorical
            values converted to integers
        """
        for col in cat_col:
            if raw[col] == "MUTATED":
                raw[col] = 1
            if raw[col] == "NOT_MUTATED":
                raw[col] = 0
        return raw

    races = list(df.Race.value_counts().index)
    races_dict = {race: id for id, race in enumerate(races)}

    def race_to_int(raw: pd.Series) -> pd.Series:
        """Converts categorical values in a dataframe to integers.

        The function takes a pandas Series as input and converts
        categorical values in the Race column to integers.

        Args:
            raw (pd.Series): Input pandas Series

        Returns:
            pd.Series: Output pandas Series with categorical
            values converted to integers
        """
        raw["Race"] = races_dict[raw["Race"]]
        return raw

    def gender_to_int(raw: pd.Series) -> pd.Series:
        """Converts categorical values in the Gender column to integers.

        The function takes a pandas Series as input and
        converts categorical values in the Gender column to integers.

        Args:
            raw (pd.Series): Input pandas Series

        Returns:
            pd.Series: Output pandas Series with
                    categorical values converted to integers
        """
        if raw["Gender"] == "Male":
            raw["Gender"] = 1
        if raw["Gender"] == "Female":
            raw["Gender"] = 0
        else:
            raw["Gender"] = 2
        return raw

    def age_to_days(raw: pd.Series) -> pd.Series:
        """Converts age at diagnosis from a string to days.

        The function takes a pandas Series as input and converts
        the Age_at_diagnosis column to an integer number of days.

        Args:
            raw (pd.Series): Input pandas Series

        Returns:
            pd.Series: Output pandas Series with
            Age_at_diagnosis converted to days
        """
        age_str = raw["Age_at_diagnosis"]
        parts = age_str.split(" ")
        if len(parts) != 4:
            if parts[0] == "--":
                raw["Age_at_diagnosis"] = None
            else:
                raw["Age_at_diagnosis"] = int(parts[0])
        else:
            # Разделение строки на года и дн
            years = int(parts[0])
            days = int(parts[2])

            # Перевод в дни (предполагаем, что в году 365 дней)
            raw["Age_at_diagnosis"] = years * 365 + days
        return raw

    diagnosis = list(df.Primary_Diagnosis.value_counts().index)
    diagnosis_dict = {diag: id for id, diag in enumerate(diagnosis)}

    def diag_to_int(raw: pd.Series) -> pd.Series:
        """
        Convert Primary_Diagnosis from string to integer.

        The function takes a pandas Series as input and converts
        the Primary_Diagnosis column to an integer using a dictionary.

        Args:
            raw (pd.Series): Input pandas Series

        Returns:
            pd.Series: Output pandas Series with Primary_Diagnosis
            converted to integer
        """
        raw["Primary_Diagnosis"] = diagnosis_dict[raw["Primary_Diagnosis"]]
        return raw

    def grade_to_int(raw: pd.Series) -> pd.Series:
        """
        Convert Grade from string to integer.

        The function takes a pandas Series as input and converts
        the Grade column to an integer using a dictionary.

        Args:
            raw (pd.Series): Input pandas Series

        Returns:
            pd.Series: Output pandas Series with Grade converted to integer
        """
        if raw["Grade"] == "LGG":
            raw["Grade"] = 0
        if raw["Grade"] == "GBM":
            raw["Grade"] = 1
        return raw

    try:
        # Замена значений
        df = df.apply(race_to_int, axis=1)
        df = df.apply(gender_to_int, axis=1)
        df = df.apply(age_to_days, axis=1)
        df = df.apply(diag_to_int, axis=1)
        df.loc[:, "IDH1":] = df.loc[:, "IDH1":].apply(cat_to_int, axis=1)
        df = df.apply(grade_to_int, axis=1)
        df["Age_at_diagnosis"].fillna(
            np.mean(df["Age_at_diagnosis"]), inplace=True
        )

        processed_file_path = (
            "/root/projects/mlops_project/data/processed/data_features.csv"
        )

        df.to_csv(processed_file_path, index=False)

        return "Данные успешно обработаны"

    except Exception as e:
        return f"Возникла при обработке данных:\n{e}"
