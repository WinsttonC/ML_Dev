from redis import Redis
from rq import Queue, Worker

redis_conn = Redis(host="localhost", port=6379, db=0)

queue = Queue(connection=redis_conn)

worker = Worker([queue], connection=redis_conn)
worker.work()
if __name__ == "__main__":
    worker.work()
