import os
from datetime import datetime, timedelta

from dotenv import load_dotenv
from fastapi import HTTPException
from jose import JWTError, jwt
from security import pwd_context, verify_password
from sqlalchemy.orm import Session

from databases.db_users import User

load_dotenv()

SECRET_KEY = os.getenv("SECRET_KEY")
ALGORITHM = os.getenv("HASH_ALGORITHM")


def get_user(db: Session, user_name: str) -> str:
    """
    Gets a user by username

    Args:
        db (Session): Session object for interacting with the database
        user_name (str): Username of the user to be retrieved

    Returns:
        str: The user with the given username, or None if no such user exists
    """
    return db.query(User).filter(User.username == user_name).first()


def authenticate_user(db: Session, username: str, password: str) -> str:
    """
    Authenticates a user by username and password

    Args:
        db (Session): Session object for interacting with the database
        username (str): Username of the user to be authenticated
        password (str): Password of the user to be authenticated

    Returns:
        str: The authenticated user, or None if the username or password is
            incorrect
    """
    user = get_user(db, username)
    if not user or not verify_password(password, user.hashed_password):
        return None
    return user


def create_user(db: Session, user: dict[str, str]) -> User:
    """
    Creates a new user in the database

    Args:
        db (Session): Session object for interacting with the database
        user (dict[str, str]): Dictionary containing the username and password
            of the new user

    Returns:
        User: The newly created user
    """
    hashed_password = pwd_context.hash(user["password"])

    db_user = User(
        username=user["username"], hashed_password=hashed_password, money=0
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_access_token(
    data: dict[str, str],
    expires_delta: timedelta,
) -> str:
    """
    Creates an access token for the given data

    Args:
        data (dict[str, str]): Data to be encoded in the access token
        expires_delta (Optional[timedelta]): Optional timedelta for when the
                                            token expires
                                            (if not provided, the token will
                                            have an expiration time 15 minutes
                                            in the future)

    Returns:
        str: The created access token
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_current_user(
    token: str,
) -> str:
    """
    Gets the current user from a token

    Args:
        token (str): The token to be validated

    Returns:
        str: The username of the current user

    Raises:
        HTTPException: If the token is invalid or has expired
    """
    credentials_exception = HTTPException(
        status_code=401,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("username")
        if username is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    return username
