from pydantic import BaseModel


class UserLogin(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class GetPrediction(BaseModel):
    token: str
    job_id: str
    model_name: str


class Prediction(BaseModel):
    token: str
    model_name: str
    file_path: str
