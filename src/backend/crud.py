from datetime import datetime

from auth import get_user
from fastapi import HTTPException
from sqlalchemy.orm import Session

from databases.db_operations import UserOperations


def add_operation(
    db: Session,
    operation_info: dict,
) -> UserOperations:
    """
    Adds a new operation to the database

    Args:
        db (Session): Session object for interacting with the database
        operation_info (dict[str, Any]): Dictionary containing the operation's
            information, including the username, message, and date of the
            operation

    Returns:
        UserOperations: The newly created operation
    """
    db_operation = UserOperations(
        username=operation_info["username"],
        message=operation_info["message"],
        date=operation_info["date"],
    )
    db.add(db_operation)
    db.commit()
    db.refresh(db_operation)
    return db_operation


def add_money(
    amount: int,
    current_user: str,
    db: Session,
    db_op: Session,
) -> dict[str, str]:
    """
    Adds money to the current user's account.

    Args:
        amount (int): The amount of money to add to the user's account.
        current_user (str): The username of the current user.
        db (Session): Session object for interacting with the database.
        db_op (Session): Session object for interacting with the database's
            operations.

    Returns:
        dict[str, str]: A dictionary containing a success message and the
            updated user's money.
    """
    user = get_user(db, current_user)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")

    if amount <= 0:
        raise HTTPException(
            status_code=400, detail="Deposit amount must be positive"
        )

    user.money += amount
    db.commit()
    db.refresh(user)
    operation_info = {
        "username": user.username,
        "message": f"Пополнение счета на {amount} кредитов.",
        "date": datetime.now(),
    }
    add_operation(db_op, operation_info)
    return {
        "message": f"Successfully added {amount} to your account.",
        "money": f"{user.money}",
    }
