import os
from datetime import datetime, timedelta

from auth import (
    authenticate_user,
    create_access_token,
    create_user,
    get_current_user,
    get_user,
)
from crud import add_money, add_operation
from dotenv import load_dotenv
from fastapi import Depends, FastAPI, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from rq.job import Job
from schemas import GetPrediction, Prediction, Token
from sqlalchemy.orm import Session
from worker.worker import queue, redis_conn  # ошибка тут

from databases.db_operations import get_db_operations
from databases.db_users import get_db_users
from models import available_models, make_prediction

load_dotenv()

ACCESS_TOKEN_EXPIRE_MINUTES = os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES")


app = FastAPI()


@app.post("/register")
def register_user(
    user: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db_users),
) -> bool:
    """
    Registers a new user

    Args:
        user: OAuth2PasswordRequestForm containing the username and password
        db: Session object for interacting with the database

    Returns:
        bool: Whether the registration was successful

    Raises:
        HTTPException: 400 if the username already exists
    """
    db_user = get_user(db, user.username)
    if db_user:
        raise HTTPException(
            status_code=400, detail="Username already registered"
        )
    else:
        user_dict = {"username": user.username, "password": user.password}
        create_user(db, user_dict)

    return True


@app.post("/token")
def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(use_cache=True),
    db: Session = Depends(get_db_users),
) -> Token:
    """
    Endpoint for logging in and obtaining an access token.

    Args:
        form_data (OAuth2PasswordRequestForm): The form data containing the
                                               username and password.
        db (Session): The database session.

    Returns:
        Token: The access token and its type.

    Raises:
        HTTPException: If the credentials are invalid.
    """

    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Invalid credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token_expires = timedelta(minutes=int(ACCESS_TOKEN_EXPIRE_MINUTES))
    access_token = create_access_token(
        data={"username": user.username}, expires_delta=access_token_expires
    )
    print(access_token)
    return Token(access_token=access_token, token_type="bearer")


@app.post("/add-money/{amount}")
def add_money_to_account(
    amount: float,  # The amount of money to add
    token: str,  # The access token
    db: Session = Depends(get_db_users),  # The database session
    db_op: Session = Depends(get_db_operations),  # The database operations s.
) -> dict[str, str]:  # A dictionary with an empty string as the value
    """
    Endpoint for adding money to the user's account.

    Args:
        amount (float): The amount of money to add.
        token (str): The access token.
        db (Session): The database session.
        db_op (Session): The database operations session.

    Returns:
        dict[str, str]: A dictionary with an empty string as the value.
    """
    current_user = get_current_user(token)

    return add_money(amount, current_user, db, db_op)


@app.post("/user_info")
def user_info(
    token: str,  # The access token
    db: Session = Depends(get_db_users),  # The database session
) -> dict[str, int]:  # A dictionary: username and money of the user
    """
    Endpoint for retrieving user information.

    Args:
        token (str): The access token.
        db (Session, optional): The database session.
                                Defaults to the result of the
                                `get_db_users` function.

    Returns:
        dict[str, int]: A dictionary containing
                        the username and money of the user.
    """
    current_user = get_current_user(token)
    user = get_user(db, current_user)
    return {"username": user.username, "money": user.money}


@app.post("/choose_model")
def models_list(
    token: str,  # The access token
    model_name: str | None = None,  # The name of the model to choose.
    db: Session = Depends(get_db_users),  # The database session
) -> str | list[str]:  # The name of the chosen model, or a list of all models
    """
    Endpoint for choosing a machine learning model.

    Args:
        token (str): The access token.
        model_name (str | None, optional): The name of the model to choose.
                                           If not specified, a list of all
                                           models is returned.
                                           Defaults to None.

    Returns:
        str | list[str]: The name of the chosen model,
                         or a list of all models if no model was chosen.
    """
    current_user = get_current_user(token)
    user = get_user(db, current_user)
    if user is None:
        raise HTTPException(status_code=404, detail="Пользователь не найден")
    if model_name is None:
        return list(available_models.keys())
    if user.money < available_models[model_name]:
        raise HTTPException(status_code=404, detail="Недостаточно средств")
    else:
        return f"Выбрана модель {model_name}"


@app.post("/prediction")
def prediction(
    input_data: Prediction,
    db: Session = Depends(get_db_users),
    db_op: Session = Depends(get_db_operations),
) -> dict:  # The job ID of the prediction
    """
    Endpoint for running a prediction.

    Args:
        input_data (Prediction): Prediction data containing the token, model
            name, and file path.
        db (Session, optional): The database session. Defaults to the result
            of the `get_db_users` function.
        db_op (Session, optional): The database operations session. Defaults
            to the result of the `get_db_operations` function.

    Returns:
        JobIdResponse: The job ID of the prediction.
    """
    token = input_data.token
    model_name = input_data.model_name
    data_path = input_data.file_path
    model_path = f"models/{model_name}.joblib"
    current_user = get_current_user(token)
    user = get_user(db, current_user)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")

    job = queue.enqueue(
        make_prediction, model_path, data_path
    )  # TODO: переписать очереди, сейчас не сработает
    return {"job_id": job.id}


@app.post("/get_prediction")
def get_prediction(
    data: GetPrediction,
    db: Session = Depends(get_db_users),
    db_op: Session = Depends(get_db_operations),
) -> dict:
    """
    Endpoint for getting the result of a prediction.

    Args:
        data (GetPrediction): Prediction data containing the token, job ID,
            and model name.
        db (Session, optional): The database session. Defaults to the result
            of the `get_db_users` function.
        db_op (Session, optional): The database operations session. Defaults
            to the result of the `get_db_operations` function.

    Returns:
        JobResultResponse: The result of the prediction or an error message.
    """
    token = data.token
    job_id = data.job_id
    model_name = data.model_name

    current_user = get_current_user(token)
    user = get_user(db, current_user)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")

    job = Job.fetch(job_id, connection=redis_conn)  # ошибка тут
    if job.is_finished:
        if "ошибка" in job.result:
            return {"result": str(job.result)}
        else:
            user.money -= available_models[model_name]
            db.commit()
            db.refresh(user)

            operation_info_1 = {
                "username": user.username,
                "message": (
                    f"Со счета списано "
                    f"{available_models[model_name]} кредитов."
                ),
                "date": datetime.now(),
            }
            add_operation(db_op, operation_info_1)
            operation_info_2 = {
                "username": user.username,
                "message": (
                    f"Выполнено предсказание с "
                    f"использованием {model_name}."
                ),
                "date": datetime.now(),
            }
            add_operation(db_op, operation_info_2)
            return {"result": str(job.result)}
    else:
        return {"result": "В процессе"}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("app:app", host="0.0.0.0", port=8080, reload=True)
