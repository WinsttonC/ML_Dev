import pandas as pd
from joblib import load

available_models = {
    "LogisticRegression": 6.0,
    "KNeighborsClassifier": 4.0,
    "GradientBoostingClassifier": 9.0,
}


def make_prediction(
    model_path: str,
    data_path: str,
) -> list:
    """
    Runs a prediction using a model and data.

    Args:
        model_path (str): The path to the model.
        data_path (str): The path to the data.

    Returns:
        np.ndarray: The prediction result.
    """
    model = load(model_path)
    data = pd.read_csv(data_path)
    try:
        prediction = model.predict(data)
        return prediction
    except Exception as e:
        return f"Произошла ошибка: {e}"
