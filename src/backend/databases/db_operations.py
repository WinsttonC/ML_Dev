from datetime import datetime

from sqlalchemy import Column, DateTime, Integer, String, create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

SQLALCHEMY_DATABASE_URL = (
    "sqlite:////root/projects/mlops_project/databases/operations.db"
)

Base = declarative_base()


class UserOperations(Base):
    __tablename__ = "user_operations"
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    message = Column(String)
    date = Column(DateTime, default=datetime.utcnow())


engine = create_engine(SQLALCHEMY_DATABASE_URL)
Base.metadata.create_all(bind=engine)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db_operations():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
