import pandas as pd


def process_dataframe(path):
    # /root/projects/mlops_project/data/raw/data.csv
    df = pd.read_csv(path)
    df.drop(columns=["Project", "Case_ID"], axis=1, inplace=True)

    df.to_csv(
        "/root/projects/mlops_project/data/processed/processed_data.csv",
        index=False,
    )
